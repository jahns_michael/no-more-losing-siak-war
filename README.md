# No More Losing SIAK WAR

- [No More Losing SIAK WAR](#no-more-losing-siak-war)
  - [Dependencies](#dependencies)
  - [Cara Penggunaan](#cara-penggunaan)
  - [Console Message](#console-message)

## Dependencies
1. `node`
2. `npm`

## Cara Penggunaan
1. Fork/Clone repo ini dengan cara 
   ```
   git clone https://gitlab.com/jahns_michael/no-more-losing-siak-war.git
   ```
2. Install dependencies dengan cara menjalankan 
   ```
   npm install
   ```
3. Buat file JSON, untuk dokumentasi ini file tersebut akan diberi nama `data.json`, namun anda boleh menamainya dengan nama apapun asalkan ber-ekstensi `.json` (diakhiri dengan `.json`).
4. File `data.json` akan berisikan
   ```javascript
   {
       "account": {
           "u": "your.sso.username",
           "p": "your.sso.password"
       },
       "courses": {
           "c[MK_K]": "CC-SKS", // mata kuliah 1
           "c[MK_K]": "CC-SKS", // mata kuliah 2
           // dan seterusnya, sesuai banyaknya mata kuliah yang anda ambil
           // baris mata kuliah terakhir jangan diberi koma di ujung
       }
   }
   ```

   Berikut penjelasannya:
   1. `"your.sso.username"` diganti dengan username SSO UI anda
   2. `"your.sso.password"` diganti dengan password SSO UI anda
   3. `"MK"` diganti dengan Kode Mata Kuliah yang ingin diambil (lihat pada gambar di bawah, ditandai dengan kode "MK")
   4. `"K"` diganti dengan Kode Kurikulum yang ingin diambil (lihat pada gambar di bawah, ditandai dengan kode "K")
   5. `"CC"` diganti dengan kode kelas yang ingin diambil (lihat pada gamabr di bawah, ditandai dengan kode "CC")
   6. `"SKS"` diganti dengan jumlah SKS kelas yang ingin diambil (lihat pada gamabr di bawah, ditandai dengan kode "SKS")

    Perhatikan gambar di bawah ini untuk lebih jelasnya.

    ![classinfo](imgs/classinfo.png)

    Pada kasus kelas pada gambar di atas, maka dapat anda tuliskan:
    ```javascript
    {
       "account": {
           "u": "your.sso.username", // dalam kasus ini diganti dengan username SSO UI Mahasiswa Jahns Michael
           "p": "your.sso.password"  // dalam kasus ini diganti dengan password SSO UI Mahasiswa Jahns Michael
       },
       "courses": {
           "c[CSGE601020_01.00.12.01-2020]": "633028-4", // mata kuliah yang berkaitan dengan gambar di atas
           // lanjut mata kuliah di selanjutnya
       }
    }
    ```

    > Jika anda tidak familiar dengan programming/javascript/json, jangan menyertakan tulisan yang diawali dengan '//' pada file anda, tulisan tersebut hanyalah komentar.
    > Sehingga setelah dibersihkan, ini adalah isi dari file anda (kasus pada gambar)
    > ```json
    > {
    >    "account": {
    >        "u": "your.sso.username",
    >        "p": "your.sso.password"
    >    },
    >    "courses": {
    >        "c[CSGE601020_01.00.12.01-2020]": "633028-4"
    >    }
    > }
    > ```
5. Pada saat war, jalankan perintah ini di terminal
   ```console
   node scrape.js ./data.json
   ```
   Perhatikan, nama dan lokasi file anda mungkin berbeda, silahkan disesuaikan.

## Console Message
Format pesan pada console ialah:
```
[ state ] message
```

Script ini menjalankan beberapa tahapan (**state**) di antaranya:
1. `[ login ]` : script mencoba login dengan data account
2. `[ changeRole ]` : script mencoba memasang ROLE menjadi Mahasiswa
3. `[ editPlan ]` : script mencoba mengisi form dengan data courses
4. `[ savePlan ]` : script mencoba mensubmit form yang sudah diisi
   
Selain itu, ada beberapa **message** di antaranya:
1. `success` : state berhasil
2. `failed` : state gagal
3. `rerun` : state dijalankan kembali

Contoh : `[ login ] success` artinya proses login berhasil