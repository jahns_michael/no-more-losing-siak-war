const rp = require("request-promise").defaults({
    jar: true,
    simple: false,
    insecure: true,
    strictSSL: false,
});
const cheerio = require("cheerio");

require("https").globalAgent.options.ca =
    require("ssl-root-cas/latest").create();
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

const filename = process.argv[2]; // argv[0]:node argv[1]:<nama_file> argv[2:]:given_argument
const userData = require(filename);

const urls = {
    login: "https://academic.ui.ac.id/main/Authentication/Index",
    role: "https://academic.ui.ac.id/main/Authentication/ChangeRole?role=Mahasiswa",
    welcome: "https://academic.ui.ac.id/main/Welcome/Index",
    editPlan: "https://academic.ui.ac.id/main/CoursePlan/CoursePlanEdit",
    savePlan: "https://academic.ui.ac.id/main/CoursePlan/CoursePlanSave",
    donePlan: "https://academic.ui.ac.id/main/CoursePlan/CoursePlanDone",
    logout: "https://academic.ui.ac.id/main/Authentication/Logout",
};

let editPlanFail = 0;
var savePlanFormData = userData.courses;

const login = () => {
    rp({
        method: "POST",
        uri: urls.login,
        form: userData.account,
        timeout: 5000,
    })
        .then((res) => {
            $ = cheerio.load(res);
            if (!$("p.error").text()) {
                console.log("[ login ] success");
                changeRole();
            } else {
                console.log("[ login ] failed\n[ login ] rerun");
                login();
            }
        })
        .catch((e) => {
            console.log("[ login ] failed\n[ login ] rerun");
            console.log(e);
            login();
        })
        .catch(() => {
            console.log("[ login ] failed\n[ login ] rerun");
            login();
        });
};

const changeRole = () => {
    rp({
        uri: urls.role,
        timeout: 5000,
    })
        .then((res) => {
            $ = cheerio.load(res);
            if ($("h2#ti_h").text() === "Selamat Datang") {
                console.log("[ changeRole ] success");
                editPlan();
            } else {
                console.log("[ changeRole ] failed: back to login");
                login();
            }
        })
        .catch((e) => {
            console.log("[ changeRole ] failed\n[ changeRole ] rerun");
            console.log(e);
            changeRole();
        })
        .catch(() => {
            console.log("[ changeRole ] failed\n[ changeRole ] rerun");
            changeRole();
        });
};

const editPlan = () => {
    rp({
        uri: urls.editPlan,
        transform: (body) => {
            return cheerio.load(body);
        },
    })
        .then(($) => {
            if (
                $("h2#ti_h").text() ===
                "Pengisian IRS - Anda tidak dapat mengisi IRS"
            ) {
                console.log(
                    "[ editPlan ] failed : Pengisian IRS - Anda tidak dapat mengisi IRS"
                );
                if (editPlanFail++ >= 10) {
                    editPlanFail = 0;
                    login();
                    console.log(
                        "[ editPlan ] failed too many times; refresh session;"
                    );
                } else {
                    editPlan();
                    console.log("[ editPlan ] rerun");
                }
            } else {
                savePlanFormData.tokens = $("input[name=tokens]").attr("value");
                savePlanFormData.submit = "Simpan IRS";
                console.log("[ editPlan ] success");
                savePlan();
            }
        })
        .catch(() => {
            console.log("[ editPlan ] failed\n[ editPlan ] rerun");
            editPlan();
        });
};

const savePlan = () => {
    rp({
        uri: urls.savePlan,
        method: "POST",
        form: savePlanFormData,
    })
        .then(() => {
            console.log("[ savePlan ] success");
            donePlan();
        })
        .catch(() => {
            console.log("[ savePlan ] failed\n[ savePlan ] rerun");
            savePlan();
        });
};

const donePlan = () => {
    rp({
        uri: urls.donePlan,
        transform: (body) => {
            return cheerio.load(body);
        },
    })
        .then(($) => {
            console.log("[ donePlan ] success\n");
            console.log("OUTPUT ----------------------- \n");
            console.log($("table.box").html() + "\n");
            console.log("OUTPUT ----------------------- \n");
            logout();
        })
        .catch(() => {
            console.log("[ donePlan ] failed\n[ donePlan ] rerun");
            donePlan();
        });
};

const logout = () => {
    rp({
        uri: urls.logout,
    })
        .then(() => {
            console.log("[ logout ] success\n");
        })
        .catch(() => {
            console.log("[ logout ] failed\n[ logout ] rerun");
            logout();
        });
};

login();
